# NYTimesMostPopularArticles

A simple app to hit the NY Times Most Popular Articles API and show a list of articles,
Implements MVVM architecture using Hilt, Retrofit, Coroutines, Flow, RoomDatabase, Database Debugging,
DataBinding and Navigation Component.


## The app has following packages:
1. **data**: It contains all the data accessing and manipulating components.
2. **di**: Dependency providing classes using Hilt.
3. **domain**: It contains data source classes and repositories.
4. **ui**: View classes along with their corresponding Presenters.
5. **utils**: Utility classes.
#### Classes have been designed in such a way that it could be inherited and maximize the code reuse.
<br>

## Library reference resources:
1. Coroutines: https://developer.android.com/kotlin/coroutines
2. Hilt: https://developer.android.com/training/dependency-injection/hilt-android
3. Retrofit: https://square.github.io/retrofit/
4. Room: https://developer.android.com/topic/libraries/architecture/room.html
5. AndroidDebugDatabase: https://github.com/amitshekhariitbhu/Android-Debug-Database
6. DataBinding: https://developer.android.com/topic/libraries/data-binding
7. Navigation Component: https://developer.android.com/guide/navigation/navigation-getting-started
<br>

