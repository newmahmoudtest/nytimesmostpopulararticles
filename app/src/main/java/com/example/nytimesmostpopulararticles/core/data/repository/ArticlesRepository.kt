package com.example.nytimesmostpopulararticles.core.data.repository

import com.example.nytimesmostpopulararticles.domain.dataSource.ArticlesLocalDataSource
import com.example.nytimesmostpopulararticles.domain.dataSource.ArticlesRemoteDataSource
import com.example.nytimesmostpopulararticles.ui.model.api.ArticlesResponse
import javax.inject.Inject

/**
 * Repository that hold more than one data source to provide to use case
 */
class ArticlesRepository @Inject constructor(
    private val articlesRemoteDataSource: ArticlesRemoteDataSource,
    private val articlesLocalDataSource: ArticlesLocalDataSource
) {

    fun getArticles() = articlesLocalDataSource.getArticles()
    suspend fun getArticlesFromRemote() = articlesRemoteDataSource.getArticles()
    fun saveArticles(article: ArticlesResponse) = articlesLocalDataSource.insertArticles(article)
}