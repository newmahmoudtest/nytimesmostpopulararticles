package com.example.nytimesmostpopulararticles.core.data.remote

import com.example.nytimesmostpopulararticles.core.data.dataUtils.Resource
import retrofit2.Response

/**
 * Base Remote data source is an abstract class that check on response
 * @property getResult is a suspend func that return a type of Resource type .
 * a generic errors can defined here to be handled in base view model like : HTTP request errors ..
 */
abstract class BaseRemoteDataSource {
    protected suspend fun <T> getResult(call: suspend () -> Response<T>): Resource<T> {
        try {
            val response = call()
            if (response.isSuccessful) {
                val body = response.body()
                if (body != null) return Resource.success(body)
            }
            return error(" ${response.code()} ${response.message()}")
        } catch (e: Exception) {
            return error("Data Not Accurate")
        }
    }


    private fun <T> error(message: String): Resource<T> {
        return Resource.error(message)
    }

}