package com.example.nytimesmostpopulararticles.core.data.remote

import com.example.nytimesmostpopulararticles.ui.model.api.ArticlesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticlesAPIService {
    @GET("v2/shared/1/facebook.json")
    suspend fun getArticles(
        @Query("api-key") apiKey: String
    ): Response<ArticlesResponse>
}