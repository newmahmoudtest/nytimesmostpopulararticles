package com.example.nytimesmostpopulararticles.core.data.dataUtils

/**
 * A wrapper for the result passed from the network layer to the upper layers
 */
data class Resource<T>(val status: Status, val data: T?, val message: String?) {

    /**
     * Enum of all response status
     * @TODO is define more status like : authorization , server error etc..
     */
    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {
        fun <T> success(data: T): Resource<T> {
            return Resource(
                Status.SUCCESS,
                data,
                null
            )
        }

        fun <T> error(message: String?, data: T? = null): Resource<T> {
            return Resource(
                Status.ERROR,
                data,
                message
            )
        }

        fun <T> loading(data: T? = null): Resource<T> {
            return Resource(
                Status.LOADING,
                data,
                null
            )
        }
    }
}