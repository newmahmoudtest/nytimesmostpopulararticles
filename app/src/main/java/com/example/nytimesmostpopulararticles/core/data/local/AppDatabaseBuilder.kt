package com.example.nytimesmostpopulararticles.core.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.Room
import com.example.nytimesmostpopulararticles.ui.model.api.ConverterClass
import com.example.nytimesmostpopulararticles.ui.model.api.Article

/**
 * Room provider class that hold the definition of Room Database
 */
@Database(entities = [Article::class], version = 1, exportSchema = false)
@TypeConverters(ConverterClass::class)
abstract class AppDatabaseBuilder : RoomDatabase() {
    abstract fun getArticles(): ArticlesDAO

    companion object {
        private var instance: AppDatabaseBuilder? = null
        fun provideRoomDatabase(appContext: Context): AppDatabaseBuilder =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(appContext).also { instance = it }
            }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabaseBuilder::class.java, "articleAppDB")
                .fallbackToDestructiveMigration()
                .build()
    }
}