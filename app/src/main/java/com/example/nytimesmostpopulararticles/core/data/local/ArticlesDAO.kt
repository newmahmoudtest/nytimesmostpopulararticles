package com.example.nytimesmostpopulararticles.core.data.local

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.room.*
import com.example.nytimesmostpopulararticles.ui.model.api.Article

/**
 * Data Access Object to define all operations needed from/to local data base
 */
@Dao
interface ArticlesDAO {

    @Query("SELECT * FROM articles")
    fun getArticles(): LiveData<List<Article>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertArticles(articles: ArrayList<Article>)

    @Transaction
    fun deleteAndInsert(articles: ArrayList<Article>) {
        deleteAll()
        insertArticles(articles)
    }

    @Query("DELETE FROM articles")
    fun deleteAll()

    @Query("Select count(*) from articles")
    fun getCount(): Int

    @Delete
    fun deleteArticles(articles: Article)
}