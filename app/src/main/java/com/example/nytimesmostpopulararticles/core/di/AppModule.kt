package com.example.nytimesmostpopulararticles.core.di

import android.content.Context
import com.example.nytimesmostpopulararticles.core.data.local.AppDatabaseBuilder
import com.example.nytimesmostpopulararticles.core.data.local.ArticlesDAO
import com.example.nytimesmostpopulararticles.core.data.remote.AppNetworkBuilder
import com.example.nytimesmostpopulararticles.core.data.remote.ArticlesAPIService
import com.example.nytimesmostpopulararticles.core.data.repository.ArticlesRepository
import com.example.nytimesmostpopulararticles.domain.dataSource.ArticlesLocalDataSource
import com.example.nytimesmostpopulararticles.domain.dataSource.ArticlesRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideAppNetworkBuilder(): Retrofit = AppNetworkBuilder().provideRetrofitClient()

    @Singleton
    @Provides
    fun provideAppDataBaseBuilder(@ApplicationContext context: Context) =
        AppDatabaseBuilder.provideRoomDatabase(context)


    @Provides
    fun provideArticlesApiService(retrofit: Retrofit): ArticlesAPIService =
        retrofit.create(ArticlesAPIService::class.java)

    @Provides
    fun provideArticlesDao(appDatabaseBuilder: AppDatabaseBuilder) =
        appDatabaseBuilder.getArticles()


    @Provides
    fun provideArticlesRemoteDataSource(articlesAPIService: ArticlesAPIService) =
        ArticlesRemoteDataSource(
            articlesAPIService
        )

    @Provides
    fun provideArticlesLocalDataSource(articlesDAO: ArticlesDAO) =
        ArticlesLocalDataSource(
            articlesDAO
        )

    @Provides
    fun provideArticlesRepository(
        remoteDataSource: ArticlesRemoteDataSource,
        localDataSource: ArticlesLocalDataSource
    ) =
        ArticlesRepository(remoteDataSource, localDataSource)

}