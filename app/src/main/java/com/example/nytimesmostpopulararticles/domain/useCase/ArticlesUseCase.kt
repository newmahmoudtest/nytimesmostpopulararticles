package com.example.nytimesmostpopulararticles.domain.useCase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.nytimesmostpopulararticles.core.data.dataUtils.Resource
import com.example.nytimesmostpopulararticles.core.data.repository.ArticlesRepository
import com.example.nytimesmostpopulararticles.ui.model.api.Article
import com.example.nytimesmostpopulararticles.ui.model.api.ArticlesResponse
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


class ArticlesUseCase @Inject constructor(private val articleRepository: ArticlesRepository) :
    BaseUseCase<String, Resource<LiveData<List<Article>>>>() {
    lateinit var mList : LiveData<MutableList<Article>>

    override fun execute(): Flow<Resource<LiveData<List<Article>>>> {
        return flow {
            // start with a loading state
            emit(Resource.loading())
            // set as a default source
            val source = articleRepository.getArticles()
            emit(Resource.success(source))
            // Get Data From Remote Data source "API"
            val responseStatus = articleRepository.getArticlesFromRemote()
            // if Data success
            if (responseStatus.status == Resource.Status.SUCCESS) {
                //Save data to local DataBase
                responseStatus.data?.let { articleRepository.saveArticles(it) }

                var tempResponse :LiveData<List<Article>> = MutableLiveData(responseStatus.data?.articles)

                //responseStatus.data?.articles?.let { mList.value?.addAll(it) }

               emit(Resource.success(tempResponse))
            } else if (responseStatus.status == Resource.Status.ERROR) {
                //Show Message
                emit(Resource.error(responseStatus.message!!, source))
            }
        }
    }


}