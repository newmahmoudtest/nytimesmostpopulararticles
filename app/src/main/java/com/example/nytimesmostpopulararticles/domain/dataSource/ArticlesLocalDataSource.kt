package com.example.nytimesmostpopulararticles.domain.dataSource

import com.example.nytimesmostpopulararticles.core.data.local.ArticlesDAO
import com.example.nytimesmostpopulararticles.ui.model.api.Article
import com.example.nytimesmostpopulararticles.ui.model.api.ArticlesResponse
import javax.inject.Inject

class ArticlesLocalDataSource @Inject constructor(private val articlesDAO: ArticlesDAO) {
    fun getArticles() = articlesDAO.getArticles()
    fun insertArticles(article: ArticlesResponse) =
        article.articles?.let { articlesDAO.deleteAndInsert(it as ArrayList<Article>) }
}