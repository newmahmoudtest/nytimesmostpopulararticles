package com.example.nytimesmostpopulararticles.domain.dataSource

import com.example.nytimesmostpopulararticles.core.data.remote.ArticlesAPIService
import com.example.nytimesmostpopulararticles.core.data.remote.BaseRemoteDataSource
import com.example.nytimesmostpopulararticles.core.utils.BusinessConstant.API_KEY
import javax.inject.Inject

class ArticlesRemoteDataSource @Inject constructor(private val articlesAPIService: ArticlesAPIService) :
    BaseRemoteDataSource() {
    suspend fun getArticles() =
        getResult { articlesAPIService.getArticles(API_KEY) }
}