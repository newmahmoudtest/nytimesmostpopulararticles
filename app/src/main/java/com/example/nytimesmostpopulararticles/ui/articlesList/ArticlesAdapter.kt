package com.example.nytimesmostpopulararticles.ui.articlesList

import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.nytimesmostpopulararticles.R
import com.example.nytimesmostpopulararticles.databinding.ItemArticleViewBinding
import com.example.nytimesmostpopulararticles.ui.model.api.Article
import kotlinx.android.synthetic.main.item_article_view.view.*

class ArticlesAdapter : RecyclerView.Adapter<ArticlesAdapter.ArticlesViewHolder>() {

    var mList: ArrayList<Article> = arrayListOf()
    fun setItems(list: ArrayList<Article>) {
        this.mList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticlesViewHolder {
        return ArticlesViewHolder(
            ItemArticleViewBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: ArticlesViewHolder, position: Int) {
        holder.bind(mList[position])
    }

    class ArticlesViewHolder(itemView: ItemArticleViewBinding) :
        RecyclerView.ViewHolder(itemView.root) {
        private lateinit var item: Article
        fun bind(item: Article) {
            this.item = item
            itemView.titleTextView.text = item.title
            itemView.authorTextView.text = item.abstractX
            itemView.dateTextView.text = item.publishedDate
        }
    }
}