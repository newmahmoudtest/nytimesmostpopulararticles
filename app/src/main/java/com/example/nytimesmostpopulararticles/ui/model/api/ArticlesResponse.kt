package com.example.nytimesmostpopulararticles.ui.model.api

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName
import com.google.gson.reflect.TypeToken

data class ArticlesResponse(
    @SerializedName("results")
    var articles: List<Article>? = listOf()
)

@Entity(tableName = "articles")
data class Article(
    @field:PrimaryKey
    var id: Long ,
    @field:ColumnInfo(name = "image_url")
    var imageUrl: String?,

    var url: String? ,
    var byline: String? ,
    var type: String? ,
    var title: String? ,

    @SerializedName("abstract")
    @field:ColumnInfo(name = "abstract")
    var abstractX: String? ,

    @SerializedName("published_date")
    @field:ColumnInfo(name = "published_date")
    var publishedDate: String?
)

object ConverterClass {
    @TypeConverter
    @JvmStatic
    fun fromString(value: String): List<Article> {
        val listType =
            object : TypeToken<List<Article>>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    @JvmStatic
    fun fromArrayList(list: List<Article>): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}