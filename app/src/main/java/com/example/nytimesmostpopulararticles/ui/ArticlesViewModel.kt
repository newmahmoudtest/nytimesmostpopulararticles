package com.example.nytimesmostpopulararticles.ui

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nytimesmostpopulararticles.core.data.dataUtils.Resource
import com.example.nytimesmostpopulararticles.domain.useCase.ArticlesUseCase
import com.example.nytimesmostpopulararticles.ui.model.api.Article
import com.example.nytimesmostpopulararticles.ui.model.api.ArticlesResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

class ArticlesViewModel @ViewModelInject constructor(private val articlesUseCase: ArticlesUseCase) :
    ViewModel() {

    var mList = MutableLiveData<List<Article>>()
    var isLoaded = MutableLiveData<Boolean>()
    var errorMessage = MutableLiveData<String>()

    fun getArticles() {
        articlesUseCase.execute()
            .flowOn(Dispatchers.IO)
            .onEach {
                when (it.status) {
                    Resource.Status.SUCCESS -> {
                        mList.postValue(it.data?.value)
                        isLoaded.postValue(true)
                    }

                    Resource.Status.ERROR -> {
                        isLoaded.postValue(true)
                        if (it.data == null) {
                            errorMessage.postValue("Check Connection")
                        } else {
                            errorMessage.postValue(it.message)
                        }
                    }
                    Resource.Status.LOADING -> {
                        isLoaded.postValue(false)
                    }
                }
            }
            .launchIn(viewModelScope)
    }
}
