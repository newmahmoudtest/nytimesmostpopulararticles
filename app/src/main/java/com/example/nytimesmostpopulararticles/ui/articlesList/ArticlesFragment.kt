package com.example.nytimesmostpopulararticles.ui.articlesList

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nytimesmostpopulararticles.databinding.FragmentArticlesBinding
import com.example.nytimesmostpopulararticles.ui.ArticlesViewModel
import com.example.nytimesmostpopulararticles.ui.model.api.Article
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ArticlesFragment : Fragment() {

    private lateinit var articlesAdapter: ArticlesAdapter
    private lateinit var binding: FragmentArticlesBinding
    private val viewModel: ArticlesViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentArticlesBinding.inflate(inflater, container, false)
        setupRecyclerView()
        viewModel.getArticles()
        initObserver()
        return binding.root
    }

    private fun setupRecyclerView() {
        articlesAdapter = ArticlesAdapter()
        binding.resultsBeanRecyclerView.adapter = articlesAdapter
        binding.resultsBeanRecyclerView.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
    }
    private fun initObserver() {
        viewModel.mList.observe(viewLifecycleOwner, Observer {
            binding.emptyTV.visibility = View.GONE
            it?.let {
                it.let {
                    binding.resultsBeanRecyclerView.visibility = View.VISIBLE
                    articlesAdapter.setItems(it as ArrayList<Article>)
                }
            } ?: run {
                binding.emptyTV.visibility = View.VISIBLE
                binding.emptyTV.text = "No Result"
            }
        })
        viewModel.isLoaded.observe(viewLifecycleOwner, Observer {
            if (it)
                binding.progressBar.visibility = View.GONE
            else
                binding.progressBar.visibility = View.VISIBLE
        })

        viewModel.errorMessage.observe(viewLifecycleOwner, Observer {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        })

    }
}