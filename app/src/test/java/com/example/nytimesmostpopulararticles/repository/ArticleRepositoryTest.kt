package com.example.nytimesmostpopulararticles.repository

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.example.nytimesmostpopulararticles.core.data.dataUtils.Resource
import com.example.nytimesmostpopulararticles.core.data.remote.BaseRemoteDataSource
import com.example.nytimesmostpopulararticles.core.data.repository.ArticlesRepository
import com.example.nytimesmostpopulararticles.domain.dataSource.ArticlesLocalDataSource
import com.example.nytimesmostpopulararticles.domain.dataSource.ArticlesRemoteDataSource
import com.example.nytimesmostpopulararticles.ui.model.api.Article
import com.example.nytimesmostpopulararticles.ui.model.api.ArticlesResponse
import com.example.nytimesmostpopulararticles.utils.createSampleArticleResponse
import com.example.nytimesmostpopulararticles.utils.createSampleArticlesResponse
import com.google.common.truth.Truth
import io.mockk.*
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@InternalCoroutinesApi
@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(androidx.test.ext.junit.runners.AndroidJUnit4::class)
class ArticleRepositoryTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    @MockK
    lateinit var articleRemoteDataSource: ArticlesRemoteDataSource

    @MockK
    lateinit var articleLocalDataSource: ArticlesLocalDataSource

    private lateinit var articlesRepository: ArticlesRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        articlesRepository = ArticlesRepository(articleRemoteDataSource, articleLocalDataSource)
    }

    @Test
    suspend fun `when loadArticleByCity called, then make sure db called`() {
        // Given
        val articleLiveData: Flow<Article> = emptyFlow()
        articleLiveData.collect {
            (createSampleArticlesResponse(2.toLong()) as ArrayList<Article>)
        }
        val mockedObserver: Observer<Resource<Article>> = mockk(relaxUnitFun = true)

        // When
        every { articleLocalDataSource.getArticles() }
        articlesRepository.getArticlesFromRemote()


        /**
         * shouldFetch == false -> loadFromDb()
         */

        // Make sure network wasn't called


        verify {
            GlobalScope.launch(Dispatchers.Default) {
                articleRemoteDataSource.getArticles() wasNot called
            }
        }

        // Make sure db called
        verify { articleLocalDataSource.getArticles() }

        // Then
        val articleEntitySlots = mutableListOf<Resource<Article>>()
        verify { mockedObserver.onChanged(capture(articleEntitySlots)) }

        val articleEntity = articleEntitySlots[0]
        Truth.assertThat(articleEntity.status).isEqualTo(Resource.Status.SUCCESS)
        Truth.assertThat(articleEntity.data?.title).isEqualTo("sas")
        Truth.assertThat(articleEntity.data?.id)
            .isEqualTo(2.toLong()) // createSampleArticleResponse(1, "sas") returns id as 1
    }

    @Test
    suspend fun `when loadArticleByCityName called, then make sure network called`() {
        // Given
        val cityName = "Cairo"
        val articleLiveData: Flow<Article> = emptyFlow()
        articleLiveData.collect {
            (createSampleArticleResponse(2.toLong()))
        }
        val mockedObserver: Observer<Resource<Article>> = mockk(relaxUnitFun = true)

        // When
        every {
            GlobalScope.launch(Dispatchers.Default) {
                articleRemoteDataSource.getArticles()
            }
        }
        every {
            articleLocalDataSource.insertArticles(
                ArticlesResponse(
                    createSampleArticlesResponse(
                        2.toLong()
                    ) as ArrayList<Article>
                )
            )
        }
        every { articleLocalDataSource.getArticles() }

        articlesRepository.getArticlesFromRemote()

        /**
         * shouldFetch == true -> createCall() -> saveCallResult() -> loadFromDb()
         */

        // Make sure network called
        verify {
            GlobalScope.launch(Dispatchers.Default) {
                articleRemoteDataSource.getArticles()
            }
        }
        // Make sure db called
        verify { articleLocalDataSource.getArticles() }

        // Then
        val articleEntitySlots = mutableListOf<Resource<Article>>()
        verify { mockedObserver.onChanged(capture(articleEntitySlots)) }

        val articleEntity = articleEntitySlots[0]

        Truth.assertThat(articleEntity.data?.title).isEqualTo("sas")
        Truth.assertThat(articleEntity.data?.id).isEqualTo(0)
    }
}