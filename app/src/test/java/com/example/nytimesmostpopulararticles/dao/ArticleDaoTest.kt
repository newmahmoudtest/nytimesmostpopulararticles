package com.example.nytimesmostpopulararticles.dao

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.nytimesmostpopulararticles.core.data.local.AppDatabaseBuilder
import com.example.nytimesmostpopulararticles.core.data.local.ArticlesDAO
import com.example.nytimesmostpopulararticles.ui.model.api.Article
import com.example.nytimesmostpopulararticles.utils.createSampleArticleResponse
import com.example.nytimesmostpopulararticles.utils.createSampleArticlesResponse
import com.google.common.truth.Truth
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config

@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class ArticleDaoTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var appDatabaseBuilder: AppDatabaseBuilder
    private lateinit var articlesDAO: ArticlesDAO

    @Before
    fun setUp() {
        appDatabaseBuilder = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            AppDatabaseBuilder::class.java)
            .allowMainThreadQueries()
            .build()

        articlesDAO = appDatabaseBuilder.getArticles()
    }

    @After
    fun closeDatabase() {
        appDatabaseBuilder.close()
    }

    @Test
    fun `insert a article to article dao`() {
        // When
        articlesDAO.insertArticles(createSampleArticlesResponse(2.toLong()) as ArrayList<Article>)
        // Then
        val value = articlesDAO.getArticles()
        Truth.assertThat(value?.value?.get(0)?.title).isEqualTo("xyz")
        Truth.assertThat(value?.value?.get(0)?.title).isEqualTo("sas")
    }

    @Test
    fun `insert two article to article dao and then delete all after this operations count must be 0`() {
        // When
        articlesDAO.insertArticles(createSampleArticlesResponse(2.toLong()) as ArrayList<Article>)
        articlesDAO.insertArticles(createSampleArticlesResponse(4.toLong()) as ArrayList<Article>)

        val value = articlesDAO.getCount()
        Truth.assertThat(value).isEqualTo(2)

        // Then
        articlesDAO.deleteAll()
        val newValue = articlesDAO.getCount()
        Truth.assertThat(newValue).isEqualTo(0)
    }



    @Test
    fun `delete and insert a article`() {
        // When
        articlesDAO.insertArticles(createSampleArticlesResponse(2.toLong()) as ArrayList<Article>)
        val count = articlesDAO.getCount()
        Truth.assertThat(count).isEqualTo(1)

        // Then
        articlesDAO.deleteAndInsert(createSampleArticlesResponse(2.toLong()) as ArrayList<Article>)
        val newCount = articlesDAO.getCount()
        val value = articlesDAO.getArticles()
        Truth.assertThat(newCount).isEqualTo(1)
        Truth.assertThat(value.value?.get(0)?.title).isEqualTo("sas")
    }

    @Test
    fun `first insert a article then delete and count must be zero`() {
        // When
        articlesDAO.insertArticles(createSampleArticlesResponse(2.toLong()) as ArrayList<Article>)
        val count = articlesDAO.getCount()
        Truth.assertThat(count).isEqualTo(1)

        // Then
        articlesDAO.deleteArticles(createSampleArticleResponse(2.toLong()))
        val newCount = articlesDAO.getCount()
        Truth.assertThat(newCount).isEqualTo(0)
    }
}

}