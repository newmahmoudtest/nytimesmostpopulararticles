package com.example.nytimesmostpopulararticles

import org.junit.runner.RunWith
import org.junit.runners.Suite
import org.robolectric.annotation.Config
import android.os.Build
import com.example.nytimesmostpopulararticles.dao.ArticleDaoTest
import com.example.nytimesmostpopulararticles.repository.ArticleRepositoryTest
import com.example.nytimesmostpopulararticles.viewModel.ArticleViewModelTest

@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(Suite::class)
@Suite.SuiteClasses(
    ArticleViewModelTest::class,
    ArticleDaoTest::class,
    ArticleRepositoryTest::class)
class TestSuite