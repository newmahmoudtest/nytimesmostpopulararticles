package com.example.nytimesmostpopulararticles.utils

import com.example.nytimesmostpopulararticles.ui.model.api.Article


// Data Generators
fun createSampleArticleResponse(id: Long): Article {
    return Article(id, "","","ss","s","sas","ds","ss")
}

fun createSampleArticlesResponse(id: Long): List<Article> {
    val mList :ArrayList<Article> = arrayListOf()
    mList.add(Article(id, "","","ss","s","sas","ds","ss"))
    mList.add(Article(id+2, "","","ss","s","sas33","ds","ss"))
    mList.add(Article(id+3, "","","ss","s","sas33","ds","ss"))
    return mList
}
