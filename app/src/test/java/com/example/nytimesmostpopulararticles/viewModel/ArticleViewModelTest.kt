package com.example.nytimesmostpopulararticles.viewModel

import android.os.Build
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.nytimesmostpopulararticles.core.data.dataUtils.Resource
import com.example.nytimesmostpopulararticles.domain.useCase.ArticlesUseCase
import com.example.nytimesmostpopulararticles.ui.ArticlesViewModel
import com.example.nytimesmostpopulararticles.ui.model.api.Article
import com.google.common.truth.Truth
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.mockk.verify
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.annotation.Config


@Config(sdk = [Build.VERSION_CODES.P])
@RunWith(AndroidJUnit4::class)
class ArticleViewModelTest {
    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()
    @MockK
    lateinit var articlesUseCase: ArticlesUseCase

    private lateinit var articlesViewModel: ArticlesViewModel

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        articlesViewModel = ArticlesViewModel(articlesUseCase)
    }

    @Test
    fun `given loading state, when setArticleParams called, then update live data for loading status`() {
        // Given
        val viewStateObserver: Observer<List<Article>> = mockk(relaxUnitFun = true)
        articlesViewModel.mList.observeForever(viewStateObserver)

        val viewStateLiveData: MutableLiveData<Resource<Article>> = MutableLiveData()
        viewStateLiveData.postValue(Resource.loading(null))

        // When
        every { articlesUseCase.execute() }
        articlesViewModel.getArticles()

        // Then
        val articleViewStateSlots = mutableListOf<List<Article>>()
        verify { viewStateObserver.onChanged(capture(articleViewStateSlots)) }

        val loadingState = articleViewStateSlots[0]
        Truth.assertThat(loadingState).isEqualTo(Resource.Status.LOADING)
    }
}